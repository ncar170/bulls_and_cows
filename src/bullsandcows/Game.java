package bullsandcows;

import bullsandcows.Computer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Game {

    int bullCount; //Creating an instance variable to count the number of Bulls for each guess
    int cowCount; //Creating an instance variable to count the number of Cows for each guess
    int turns; //Creating an instance variable to count the number of turns during each game
    int[] compCode;
    int[] userCode;

    /* The following lists are created to keep a list of the respective codes, and number of bulls/cows for each
    round which are then used in the printResults() method to print the results to file */

    List<Integer> turnsList = new ArrayList<>();
    List<int[]> userGuessList = new ArrayList<>();
    List<Integer> userBullsList = new ArrayList<>();
    List<Integer> userCowsList = new ArrayList<>();
    List<int[]> compGuessList = new ArrayList<>();
    List<Integer> compBullsList = new ArrayList<>();
    List<Integer> compCowsList = new ArrayList<>();


    public Game(int bullCount, int cowCount, int turns, int[]compCode, int[]userCode){
        this.bullCount = bullCount;
        this.cowCount = cowCount;
        this.turns = turns;
        this.compCode = compCode;
        this.userCode = userCode;
    }

    public Game(){

    }

    Computer computer = new Computer();
    Player player = new Player();

    public abstract void manualGame(int[] randomCompCode, int[] secretCode, int userTurns);

    public abstract void automaticGame(int [] randomCompCode, int[] secretCode, int userTurns, List<int[]> guessList);


        /* The method getBulls() calculates the number of bulls for the two codes (computerCode and playerGuess) by
            comparing values in the two codes at the same position; it then returns a count of Bulls which is
            assigned to the variable bullCount */

    int getBulls(int[] int1, int [] int2){
        int countBulls = 0;
        int i = 0;
        while(i < int1.length && i < int2.length){
            if(int1[i] == int2[i]){
                countBulls++;
            }
            i++;
        }
        return countBulls;
    }

        /* The method getCows() calculates the number of cows for the two codes (computerCode and playerGuess) by
            comparing values in the two codes that are in both codes but in different positions; it then returns a
            count of Cows which is assigned to the variable cowCount */

    int getCows(int[] int1, int[] int2) {
        int countBulls = 0;
        int countCows = 0;
        for (int i = 0; i < int1.length; i++) {
            if (int1[i] == int2[i]){
                countBulls++;
            } else {
                for (int j = 0; j < int1.length; j++) {
                    if (int1[i] == int2[j]){
                        countCows++;
                    }
                }
            }
        }
        return countCows;
    }

    boolean hasPrinted () {
        System.out.println("-------------");
        System.out.println("Would you like to print the results of this game?\nYes(Y) or No(N)");
        String choice = Keyboard.readInput();
        return choice.equalsIgnoreCase("yes") || choice.equalsIgnoreCase("y");
    }

    void printResults(List<Integer> turnsList, List<Integer> userBulls, List<Integer> userCows,
                                List<int[]> userList, List<int[]> compList, List<Integer> compBulls,
                                List<Integer> compCows, int[] secretCode, int[] randomCompCode) {

        System.out.println("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File myFile = new File(fileName);

        try (PrintWriter pw = new PrintWriter(new FileWriter(fileName))) {

            pw.println("Bulls and Cows game result: ");
            pw.println("Your code: " + Arrays.toString(secretCode));
            pw.println("Computer's code: " + Arrays.toString(randomCompCode));
            pw.println("---");

            int uBulls = 0;
            int cBulls = 0;

            for (int i = 0; i < turnsList.size(); i++) {
                int round = turnsList.get(i) + 1;
                uBulls = userBulls.get(i);
                int uCows = userCows.get(i);
                cBulls = compBulls.get(i);
                int cCows = compCows.get(i);
                pw.println("Round " + round);
                pw.print("You guessed ");
                pw.print(Arrays.toString(userList.get(i)));
                pw.println(" which scored " + uBulls + " bulls and " + uCows + " cows.");
                if (uBulls == 4){
                    break;
                }
                pw.print("The computer guessed ");
                pw.print(Arrays.toString(compList.get(i)));
                pw.println(" which scored " + cBulls + " bulls and " + cCows + " cows.");
                pw.println("---");
            }
            if (uBulls == 4){
                pw.println("You scored four bulls and won the game!");
            } else if (cBulls == 4){
                pw.println("The computer scored four bulls and won the game.");
            } else {
                pw.println("Neither of you scored four bulls so the game ended in a a tie.");
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

}
