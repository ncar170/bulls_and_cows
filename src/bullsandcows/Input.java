package bullsandcows;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Input {

    List<int[]> playerFile;

    public Input(){

    }

    /* The getPlayerFile() method asks the user to enter a file name, which is passed into the loadGuesses() method
     and then gets back the list read in the loadGuesses() method and assigns it to guessList - which is then used
     in the game classes as the user guesses when choosing automatically*/

    List<int[]> getPlayerFile () {

        System.out.print("Enter a file name: ");
        String guessFile = Keyboard.readInput();
        System.out.println("--------------------");
        if (guessFile.equalsIgnoreCase("quit")){
            System.exit(0);
        }
        return loadGuesses(guessFile);
    }

    /* The loadGuesses() method reads the text file that the user uploads, and converts each line (i.e. four-digit
    * code) into an array of integers which is then passed back into the getPlayerFile() method*/

    private List <int []> loadGuesses (String fileName){

        List<int []> loadGuessList = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                int [] digits = new int[4];
                for (int i = 0; i < line.length(); i++){
                    digits[i] = line.charAt(i) - '0';
                }
                loadGuessList.add(digits);
            }
        } catch (FileNotFoundException f){
            System.out.println("File not found - please re-enter with correct file name or type quit to exit the program.");
            return getPlayerFile();
        } catch (IOException e){
            System.out.println("Incorrect file name - Please re-enter with correct file name or type quit to exit the program.");
            return getPlayerFile();
        }
        return loadGuessList;

    }
}
