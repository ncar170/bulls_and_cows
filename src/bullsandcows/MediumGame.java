package bullsandcows;

import bullsandcows.Computer;
import bullsandcows.Game;
import bullsandcows.Input;

import java.util.*;

public class MediumGame extends Game {

    public MediumGame(int bullCount, int cowCount, int turns, int[]compCode, int[]userCode) {
        super(bullCount, cowCount, turns, compCode, userCode);
    }

    public MediumGame() {

    }

    private Set<int[]> codeSet;

    @Override
    public void manualGame(int [] randomCompCode, int[] secretCode, int userTurns) {

        /* A set is created which adds randomly created codes from the computer class - as a set it will ensure that
        there are no duplicate codes. It is then converted to a list and shuffled and then each time through the
        loop it will get a new code from the list (which should ensure that the computer cannot guess the same
        code twice)*/

        codeSet = new HashSet<>();
        while (codeSet.size() < 5040) { //number of unique four digit codes
            codeSet.add(computer.getComputerCode());
        }
        List<int[]> compList1 = new ArrayList<>(codeSet);
        Collections.shuffle(compList1);

        int count = 0; //initialise a variable count to 0, which is then used as the index when getting the code

        while (turns < userTurns) {
            int[] userGuess = player.getPlayerGuess("Your guess: ");
            userGuessList.add(userGuess);
            bullCount = getBulls(randomCompCode, userGuess);
            userBullsList.add(bullCount);
            cowCount = getCows(randomCompCode, userGuess);
            userCowsList.add(cowCount);
            System.out.println("Result: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println();
            if (bullCount == 4) {
                System.out.println("Four Bulls!!! You guessed the code correctly and win the game!");
                break;
            }

            int[] medCompCode = compList1.get(count); //count is used as the index of the element in the list
            count++; //count is implemented by one each time so next time in the loop is the next element in the list
            compGuessList.add(medCompCode);
            System.out.print("Computer guess: ");
            Arrays.stream(medCompCode).forEach(System.out::print);
            bullCount = getBulls(medCompCode, secretCode);
            compBullsList.add(bullCount);
            cowCount = getCows(medCompCode, secretCode);
            compCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println("-----------------------------");
            if (bullCount == 4) {
                System.out.println("The computer picked four Bulls! Unfortunately, that mean's you have lost the game!");
                break;
            }
            turnsList.add(turns);
            turns++;
            if (turns == userTurns) {
                System.out.print("It's a tie! Neither you nor the computer were able to guess the number of bulls and cows - the computer's secret code was: ");
                Arrays.stream(randomCompCode).forEach(System.out::print);
                System.out.println("\n-----------------------------------");
            }
        }

        boolean print = hasPrinted();
        if (print){
            printResults(turnsList, userBullsList, userCowsList, userGuessList, compGuessList,
                    compBullsList, compCowsList, secretCode, randomCompCode);
        }
    }
    @Override
    public void automaticGame(int [] randomCompCode, int[] secretCode, int userTurns, List<int[]> guessList) {

        codeSet = new HashSet<>();
        while (codeSet.size() < 5040) {
            codeSet.add(computer.getComputerCode());
        }
        List<int[]> compList1 = new ArrayList<>(codeSet);
        Collections.shuffle(compList1);

        int userCount = 0; //initialise a variable count to 0, which is then used as the index when getting the code
        int compCount = 0; //initialise a variable count to 0, which is then used as the index when getting the code

        while (turns < userTurns) {
            int[] userGuess = guessList.get(userCount);
            userCount++;
            userGuessList.add(userGuess);
            System.out.print("Your guess: ");
            Arrays.stream(userGuess).forEach(System.out::print);
            bullCount = getBulls(randomCompCode, userGuess);
            userBullsList.add(bullCount);
            cowCount = getCows(randomCompCode, userGuess);
            userCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println();
            if (bullCount == 4) {
                System.out.println("Four Bulls!!! You guessed the code correctly and win the game!");
                break;
            }

            int[] medCode = compList1.get(compCount); //count is used as the index of the element in the list
            compCount++; //count is implemented by one each time so next time in the loop is the next element in the list
            compGuessList.add(medCode);
            System.out.print("Computer guess: ");
            Arrays.stream(medCode).forEach(System.out::print);
            bullCount = getBulls(medCode, secretCode);
            compBullsList.add(bullCount);
            cowCount = getCows(medCode, secretCode);
            compCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println("-----------------------------");
            if (bullCount == 4) {
                System.out.println("The computer picked four Bulls! Unfortunately, that mean's you have lost the game!");
                break;
            }
            turnsList.add(turns);
            turns++;
            if (turns == userTurns) {
                System.out.print("It's a tie! Neither you nor the computer were able to guess the number of bulls and cows - the computer's secret code was: ");
                Arrays.stream(randomCompCode).forEach(System.out::print);
                System.out.println("\n-----------------------------------");
            }
        }

        boolean print = hasPrinted();
        if (print){
            printResults(turnsList, userBullsList, userCowsList, userGuessList, compGuessList,
                    compBullsList, compCowsList, secretCode, randomCompCode);
        }
    }

}
