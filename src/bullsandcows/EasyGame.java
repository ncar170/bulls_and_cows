package bullsandcows;

import bullsandcows.Computer;

import java.util.*;

public class EasyGame extends Game {

    public EasyGame(int bullCount, int cowCount, int turns, int[] compCode, int[] userCode) {
        super(bullCount, cowCount, turns, compCode, userCode);
    }

    public EasyGame() {

    }


    @Override
    public void manualGame(int[] randomCompCode, int[] secretCode, int userTurns) {

        /* the manualGame() method  loops through the game seven times (or however many turns the user selects,
         asking the user to enter a four digit code each time and counting the number of bulls and cows.
         If bulls equals four, then the loop will break, otherwise it will end after x turns */


        while (turns < userTurns) {
            int[] userGuess = player.getPlayerGuess("Your guess: ");
            userGuessList.add(userGuess); //adds the users guess for each turn of the game to the list created in the bullsandcows.Game class
            bullCount = getBulls(randomCompCode, userGuess); //calls the getBulls method to get the number of Bulls
            userBullsList.add(bullCount);
            cowCount = getCows(randomCompCode, userGuess); //calls the getCows method to get the number of Cows
            userCowsList.add(cowCount);
            System.out.println("Result: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println();
            if (bullCount == 4) {
                System.out.println("Four Bulls!!! You guessed the code correctly and win the game!");
                break;
            }

            int[] easyCompCode = computer.getComputerCode(); //gets a random code from the computer for each turn
            compGuessList.add(easyCompCode); //adds the compCode to the list created in the bullsandcows.Game class
            System.out.print("Computer guess: ");
            Arrays.stream(easyCompCode).forEach(System.out::print);
            bullCount = getBulls(easyCompCode, secretCode);
            compBullsList.add(bullCount);
            cowCount = getCows(easyCompCode, secretCode);
            compCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println("-----------------------------");
            if (bullCount == 4) {
                System.out.println("The computer picked four Bulls! Unfortunately, that mean's you have lost the game.");
                break;
            }
            turnsList.add(turns);
            turns++;
            if (turns == userTurns) {
                System.out.print("It's a tie! Neither you nor the computer were able to guess the number of bulls and cows - the computer's secret code was: ");
                Arrays.stream(randomCompCode).forEach(System.out::print);
                System.out.println("\n-----------------------------------");
            }
        }

        /* Calls the hasPrinted() method in the Game Class - if answer returns true then the printResults() method
         * is called which prints out the results to a text file */

        boolean print = hasPrinted();
        if (print){
            printResults(turnsList, userBullsList, userCowsList, userGuessList, compGuessList,
                    compBullsList, compCowsList, secretCode, randomCompCode);
        }
    }

    @Override
    public void automaticGame(int[] randomCompCode, int[] secretCode, int userTurns, List<int[]> guessList) {

        /* the automaticGame() method  loops through the game seven times (or however many turns the user selects),
         and gets an index (i.e. 0, 1, 2...) from the supplied file each time. If bulls equals four, then the loop
         will break, otherwise it will end after x turns */

        int userCount = 0;

        while (turns < userTurns) {
            int[] userGuess = guessList.get(userCount); //gets code from the supplied file
            userCount++; //count is implemented each time so the next time round the loop it gets the next index from the file
            userGuessList.add(userGuess);
            System.out.print("Your guess: ");
            Arrays.stream(userGuess).forEach(System.out::print);
            bullCount = getBulls(randomCompCode, userGuess);
            userBullsList.add(bullCount);
            cowCount = getCows(randomCompCode, userGuess);
            userCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println();
            if (bullCount == 4) {
                System.out.println("Four Bulls!!! You guessed the code correctly and win the game!");
                break;
            }

            int[] easyCompCode = computer.getComputerCode();
            compGuessList.add(easyCompCode);
            System.out.print("Computer guess: ");
            Arrays.stream(easyCompCode).forEach(System.out::print);
            bullCount = getBulls(easyCompCode, secretCode);
            compBullsList.add(bullCount);
            cowCount = getCows(easyCompCode, secretCode);
            compCowsList.add(cowCount);
            System.out.println("\nResult: " + bullCount + " Bulls and " + cowCount + " Cows.");
            System.out.println("-----------------------------");
            if (bullCount == 4) {
                System.out.println("The computer picked four Bulls! Unfortunately, that mean's you have lost the game!");
                break;
            }
            turnsList.add(turns);
            turns++;
            if (turns == userTurns) {
                System.out.print("It's a tie! Neither you nor the computer were able to guess the number of bulls and cows - the computer's secret code was: ");
                Arrays.stream(randomCompCode).forEach(System.out::print);
                System.out.println("\n-----------------------------------");
            }
        }
        boolean print = hasPrinted();
        if (print){
            printResults(turnsList, userBullsList, userCowsList, userGuessList, compGuessList,
                    compBullsList, compCowsList, secretCode, randomCompCode);
        }
    }
}


