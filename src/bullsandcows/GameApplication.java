package bullsandcows;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameApplication {

    private static final int EASY = 1; //These constant variables are included to allow the user to select either
    private static final int MEDIUM = 2; //the number that corresponds to the level, or the actual word (i.e. easy)
    private static final int HARD = 3; //when they are selecting what level of game to play

    private static final int MANUAL = 1; //These constant variables are included to allow the user to select either
    private static final int AUTOMATIC = 2;//the number or the actual word (i.e. manual) when selecting game type


    public void start() {

        Game easy = new EasyGame();
        Game medium = new MediumGame();
//        Game hard = new HardGame();
        Input input = new Input();
        Player player = new Player();
        Computer computer = new Computer();

        int levelSelected = getLevel(); //gets the level the user has selected
        int gameType = getGameType(); //gets the game type (i.e. manual or auto) the user has selected

        /* if the user selects gameType 2 (i.e. automatic), it will call the getPlayerFile() method from the input
        * class, which is then passed in as a parameter when calling the automaticGame() method*/

        List<int[]> guessList = null;
        if (gameType == 2) {
            guessList = new ArrayList<>(input.getPlayerFile());
        }

        int turnsSelected = getTurns(); //gets the number of turns the user has selected to play

        System.out.println("-----------------------------");
        int[] randomCompCode = computer.getComputerCode(); //gets the randomly generated computer code
        int[] secretCode = player.getPlayerGuess("Enter a four digit secret code: "); //gets user's secret code
        System.out.print("-----------------------  ");
        Arrays.stream(secretCode).forEach(System.out::print);
        System.out.println("  -----------------------");
        System.out.println("Now try and guess the computer's secret code! ");
        System.out.println("-----------------------------");

        /* the following section determines which class (i.e. easy, medium, hard) and which method within that class
        * (i.e. manualGame or autoGame) is called*/

        if (levelSelected == 1 && gameType == 1) {
            easy.manualGame(randomCompCode, secretCode, turnsSelected);
        } else if (levelSelected == 1 && gameType == 2) {
            easy.automaticGame(randomCompCode, secretCode, turnsSelected, guessList);
        } else if (levelSelected == 2 && gameType == 1) {
            medium.manualGame(randomCompCode, secretCode, turnsSelected);
        } else if (levelSelected == 2 && gameType == 2) {
            medium.automaticGame(randomCompCode, secretCode, turnsSelected, guessList);
        } else {
            System.out.println("Hard level is still under construction. Please choose another level.");
            start();
        }
    }

    /* The getLevel() method asks the user to select what level they wish to play - the result is then passed back
    * into the start() method and used to choose which Game class should be called*/

    private int getLevel() {
        System.out.println("Please choose a level to play: \n1. Easy \n2. Medium \n3. Hard");
        String choice = Keyboard.readInput();
        int levelChoice = 0;
        try {
            levelChoice = Integer.parseInt(choice);
            if (levelChoice >=1 && levelChoice <= 3){
                return levelChoice;
            } else {
                System.out.println("Please enter a valid level: ");
                return getLevel();
            }
        } catch (NumberFormatException e) {
            if (choice.equalsIgnoreCase("easy") ){
                return EASY;
            }   else if (choice.equalsIgnoreCase("medium")) {
                return MEDIUM;
            }   else if (choice.equalsIgnoreCase("hard")) {
                return HARD;
            }   else {
                System.out.println("Error: Invalid input.");
                getLevel();
            }
        }
        return levelChoice;
    }

    /* The getGameType() method asks the user to select what type of game they wish to play - the result is then passed
    * back into the start() method and used to choose which game play method should be called in the corresponding
    * class*/

    private int getGameType () {
        System.out.println("Please choose whether you would like to guess either \n1. Manually, or \n2. Automatically");
        String choice = Keyboard.readInput();
        int gameType = 0;
        try {
            gameType = Integer.parseInt(choice);
            if (gameType >= 1 && gameType <= 2) {
                return gameType;
            } else {
                System.out.println("Please enter a valid game type: ");
                return getGameType();
            }
        } catch (NumberFormatException e) {
            if (choice.equalsIgnoreCase("manual")) {
                return MANUAL;
            } else if (choice.equalsIgnoreCase("automatic")) {
                return AUTOMATIC;
            } else {
                System.out.println("Error: Invalid input.");
                getGameType();
            }
        }
        return gameType;
    }

    private int getTurns () {
        System.out.print("How many turns would you like to play: ");
        int userTurns = Integer.parseInt(Keyboard.readInput());
        if (userTurns > 15) {
            System.out.print("You can not choose more than 15 turns - ");
            return getTurns();
        }
        return userTurns;
    }

    public static void main(String[] args) {
        GameApplication p = new GameApplication();
        p.start();
    }
}
