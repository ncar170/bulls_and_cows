package bullsandcows;

import java.util.*;
import java.util.stream.Stream;

public class Computer {

    int[] computerCode;

    public Computer() {

    }

    /* The getComputerCode() method gets a random set of 4 numbers (it uses a set so there can be no duplicates),
        and then converts the set into an array of four integers, which is used in the game */

    int[] getComputerCode() {
        Set<Integer> compCode = new HashSet<>();
        int number;
        while (compCode.size() < 4) {
            number = (int) (Math.random() * (9 - 0) + 0);
            compCode.add(number);
        }
        /* The following converts the set into a list, which can then be shuffled so that the order of the numbers
        * in the code is not in a sequential order. (even though a hashset doesn't guarantee order, I found that most
         of the time the code still seemed to be in ascending order so shuffling it made sense)*/
        List<Integer> compList = new ArrayList<>(compCode);
        Collections.shuffle(compList);

        return createArrayFromCode(compList);
    }

    /* The createArrayFromCode() method converts the list into an array of ints, which is then passed through to the game
        classes and used as the computers code in the gameplay*/

    private int[] createArrayFromCode(List<Integer> compList){
        int[] codeArray = new int[compList.size()];
        int index = 0;
        for (Integer i : compList) {
            codeArray[index++] = i;
        }
        return codeArray;
    }

}
